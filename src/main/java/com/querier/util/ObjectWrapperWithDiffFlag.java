package com.querier.util;

public class ObjectWrapperWithDiffFlag {
	public Object object;
	public boolean isDifferent;
	
	public ObjectWrapperWithDiffFlag(Object object, boolean isDifferent) {
		super();
		this.object = object;
		this.isDifferent = isDifferent;
	}
	
	@Override
	public String toString() {
		return this.object != null ? this.object.toString() : "null";
	}
	
}
