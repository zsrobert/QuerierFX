package com.querier.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.util.Callback;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.querier.MainApp;

@Component
public class SpringFXMLLoader implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	protected Object controller;

	public <E> E load(String url) throws IOException {
		InputStream fxmlStream = MainApp.class.getResourceAsStream(url);
		FXMLLoader loader = new FXMLLoader();

		loader.setControllerFactory(new Callback<Class<?>, Object>() {
			@Override
			public Object call(Class<?> clazz) {
				SpringFXMLLoader.this.controller = applicationContext
						.getBean(clazz);
				return SpringFXMLLoader.this.controller;
			}
		});

		return loader.load(fxmlStream);
	}

	@SuppressWarnings("unchecked")
	public <E> E getController() {
		return (E) this.controller;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public <E> E load(URL resource) throws IOException {
		FXMLLoader loader = new FXMLLoader();

		loader.setControllerFactory(new Callback<Class<?>, Object>() {
			@Override
			public Object call(Class<?> clazz) {
				SpringFXMLLoader.this.controller = applicationContext
						.getBean(clazz);
				return SpringFXMLLoader.this.controller;
			}
		});
		
		loader.setLocation(resource);
		
		return loader.load();
		// return load(resource.getFile());
	}

}
