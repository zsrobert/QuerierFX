package com.querier;

import java.io.IOException;
import java.util.Collection;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.querier.configuration.AppConfig;
import com.querier.controller.BaseSceneController;
import com.querier.util.SpringFXMLLoader;

public class MainApp extends Application {

	private static final String APP_NAME = "Querier";
	private Stage primaryStage;

	private AbstractApplicationContext context;

	@Override
	public void start(Stage primaryStage) {
		Platform.setImplicitExit(true);

		this.context = new AnnotationConfigApplicationContext(AppConfig.class);

		this.primaryStage = primaryStage;
		this.primaryStage.setTitle(APP_NAME);

		Collection<BaseSceneController> controllers = this.context
				.getBeansOfType(BaseSceneController.class).values();

		for (BaseSceneController controller : controllers) {
			controller.setPrimaryStage(this.primaryStage);
		}

		
		initRootView(this.context.getBean(SpringFXMLLoader.class));
	}

	/**
	 * Initializes the root view.
	 */
	private void initRootView(SpringFXMLLoader loader) {
		try {
			BorderPane connectionsView = loader
					.load("/com/querier/view/ConnectionsView.fxml");

			Scene scene = new Scene(connectionsView);

			this.primaryStage.setScene(scene);
			this.primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stop() throws Exception {
		this.context.close();
		super.stop();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
