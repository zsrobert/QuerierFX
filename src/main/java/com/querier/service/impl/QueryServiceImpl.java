package com.querier.service.impl;

import javafx.collections.ObservableList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querier.model.Query;
import com.querier.repository.def.QueryRepository;
import com.querier.service.def.QueryService;

@Service
public class QueryServiceImpl implements QueryService {
	
	@Autowired
	private QueryRepository repository;
	
	@Override
	public Query findOne(Long id) {
		return this.repository.findOne(id);
	}

	@Override
	public ObservableList<Query> findAll() {
		return this.repository.findAll();
	}

	@Override
	public Query save(Query item) {
		return this.repository.save(item);
	}

	@Override
	public void delete(Long id) {
		this.repository.delete(id);
	}

	public QueryRepository getRepository() {
		return repository;
	}

	public void setRepository(QueryRepository repository) {
		this.repository = repository;
	}

}
