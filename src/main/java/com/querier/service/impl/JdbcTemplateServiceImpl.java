package com.querier.service.impl;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.querier.model.Connection;
import com.querier.service.def.JdbcTemplateService;

@Service
public class JdbcTemplateServiceImpl implements JdbcTemplateService {
	
	private static final RowMapper<Map<String, Object>> DEFAULT_ROW_MAPPER = new RowToStringMapResultSetMapper();
	private JdbcTemplate leftTemplate;
	private JdbcTemplate rightTemplate;

	@Override
	public void connectLeft(Connection connection) {
		connect(connection, true);
	}

	@Override
	public void connectRight(Connection connection) {
		connect(connection, false);
	}

	private void connect(Connection connection, boolean isLeft) {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		String url = connection.getHostname() + ":" + connection.getPort()
				+ "/" + connection.getSchema();
		ds.setUrl("jdbc:mysql://" + url);
		ds.setUsername(connection.getUsername());
		ds.setPassword(connection.getPassword());

		JdbcTemplate template = new JdbcTemplate(ds);

		if (isLeft) {
			this.leftTemplate = template;
		} else {
			this.rightTemplate = template;
		}
	}

	@Override
	public List<Map<String, Object>> doQueryOnLeft(String query) throws SQLException {
		return doQuery(query, leftTemplate);
		
	}

	@Override
	public List<Map<String, Object>> doQueryOnRight(String query) throws SQLException {
		return doQuery(query, rightTemplate);
	}
	
	private List<Map<String, Object>> doQuery(String query, JdbcTemplate template) throws SQLException {
		if (template != null) {
			return template.query(query, DEFAULT_ROW_MAPPER);
		} else {
			throw new IllegalStateException("You must connect to a database in order to query!");
		}
	}
	
	private static class RowToStringMapResultSetMapper implements RowMapper<Map<String, Object>> {
		@Override
		public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			Map<String, Object> mapOfColValues = new LinkedCaseInsensitiveMap<Object>(columnCount);
			for (int i = 1; i <= columnCount; i++) {
				String key = JdbcUtils.lookupColumnName(rsmd, i);
				Object val = JdbcUtils.getResultSetValue(rs, i);
				mapOfColValues.put(key, val);
			}
			
			return mapOfColValues;
		}
	}
}
