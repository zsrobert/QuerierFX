package com.querier.service.impl;

import javafx.collections.ObservableList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querier.model.Connection;
import com.querier.repository.def.ConnectionRepository;
import com.querier.service.def.ConnectionService;

@Service
public class ConnectionServiceImpl implements ConnectionService {
	
	@Autowired
	private ConnectionRepository repository;
	
	@Override
	public Connection findOne(Long id) {
		return this.repository.findOne(id);
	}

	@Override
	public ObservableList<Connection> findAll() {
		return this.repository.findAll();
	}

	@Override
	public Connection save(Connection item) {
		return this.repository.save(item);
	}

	@Override
	public void delete(Long id) {
		this.repository.delete(id);
	}

	public ConnectionRepository getRepository() {
		return repository;
	}

	public void setRepository(ConnectionRepository repository) {
		this.repository = repository;
	}

}
