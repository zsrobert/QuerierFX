package com.querier.service.def;

import javafx.collections.ObservableList;

public interface BaseService<E> {
	public E findOne(Long id);

	public ObservableList<E> findAll();

	public E save(E item);

	public void delete(Long id);
}
