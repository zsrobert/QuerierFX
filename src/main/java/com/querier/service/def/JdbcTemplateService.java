package com.querier.service.def;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.querier.model.Connection;

public interface JdbcTemplateService {
	public void connectLeft(Connection connection) throws SQLException;
	public void connectRight(Connection connection) throws SQLException;
	public List<Map<String, Object>> doQueryOnLeft(String query) throws SQLException;
	public List<Map<String, Object>> doQueryOnRight(String query) throws SQLException;
}
