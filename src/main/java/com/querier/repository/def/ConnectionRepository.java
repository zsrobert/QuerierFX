package com.querier.repository.def;

import com.querier.model.Connection;

public interface ConnectionRepository extends CrudRepository<Connection> {

}
