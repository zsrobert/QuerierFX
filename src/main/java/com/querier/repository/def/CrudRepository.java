package com.querier.repository.def;

import javafx.collections.ObservableList;

import com.querier.model.BaseModel;

public interface CrudRepository<E extends BaseModel> {

	public E findOne(Long id);

	public ObservableList<E> findAll();

	public E save(E item);

	public void delete(Long id);
}
