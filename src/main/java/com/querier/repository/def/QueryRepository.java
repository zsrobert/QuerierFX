package com.querier.repository.def;

import com.querier.model.Query;

public interface QueryRepository extends CrudRepository<Query> {

}
