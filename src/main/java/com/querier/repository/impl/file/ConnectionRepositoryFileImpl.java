package com.querier.repository.impl.file;

import org.springframework.stereotype.Repository;

import com.querier.model.Connection;
import com.querier.repository.def.ConnectionRepository;

@Repository
public class ConnectionRepositoryFileImpl extends
		BaseRepositoryFileImpl<Connection> implements ConnectionRepository {

}
