package com.querier.repository.impl.file;

import org.springframework.stereotype.Repository;

import com.querier.model.Query;
import com.querier.repository.def.QueryRepository;

@Repository
public class QueryRepositoryFileImpl extends BaseRepositoryFileImpl<Query>
		implements QueryRepository {

}
