package com.querier.repository.impl.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import com.querier.model.BaseModel;
import com.querier.repository.def.CrudRepository;

public class BaseRepositoryFileImpl<E extends BaseModel> implements
		CrudRepository<E> {

	private static final String LIST_FILE_EXTENSION = ".list";
	private Map<Long, E> items;

	/**
	 * Helper list of all items to allow easier integration with JavaFX.
	 */
	private ObservableList<E> itemsList;
	private AtomicLong sequence = new AtomicLong(1);

	@Override
	public E findOne(Long id) {
		if (this.items == null) {
			initItems();
		}

		return items.get(id);
	}

	@Override
	public ObservableList<E> findAll() {
		if (this.items == null) {
			initItems();
		}

		return this.itemsList;
	}

	@Override
	public E save(E item) {
		if (this.items == null) {
			initItems();
		}

		if (item.getId() == null) {
			item.setId(this.sequence.getAndIncrement());

		}

		items.put(item.getId(), item);

		itemsList.remove(item);
		itemsList.add(item);

		saveItems();

		return item;
	}

	@Override
	public void delete(Long id) {
		if (this.items == null) {
			initItems();
		}

		if (this.items.containsKey(id)) {
			E item = this.items.get(id);

			this.items.remove(id);
			this.itemsList.remove(item);

			saveItems();
		}
	}

	private void saveItems() {
		String className = getGenericName();

		File file = new File(className + LIST_FILE_EXTENSION);

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}

		try {
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(this.items);

			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void initItems() {
		String className = getGenericName();

		File file = new File(className + LIST_FILE_EXTENSION);

		if (file.exists()) {
			try {
				FileInputStream fis = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(fis);
				Map<Long, E> list = (Map<Long, E>) ois.readObject();

				this.items = new HashMap<Long, E>();
				this.items.putAll(list);
				this.itemsList = FXCollections.observableArrayList(this.items
						.values());

				Optional<E> maxItem = this.items.values().stream()
						.max((o1, o2) -> (int) (o1.getId() - o2.getId()));

				if (maxItem.isPresent()) {
					this.sequence = new AtomicLong(maxItem.get().getId() + 1);
				}

				ois.close();
				fis.close();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			this.items = new HashMap<Long, E>();
			this.itemsList = FXCollections.observableArrayList();
		}
	}

	@SuppressWarnings("unchecked")
	private String getGenericName() {
		return ((Class<E>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0])
				.getSimpleName();
	}

}
