package com.querier.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.querier.repository.impl.file",
		"com.querier.service.impl", "com.querier.controller",
		"com.querier.util" })
public class AppConfig {

}
