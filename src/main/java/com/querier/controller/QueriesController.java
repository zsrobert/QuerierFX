package com.querier.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.querier.service.def.JdbcTemplateService;
import com.querier.util.SpringFXMLLoader;

@Controller
public class QueriesController implements BaseSceneController {

	protected Stage primaryStage;

	@Autowired
	private JdbcTemplateService jdbcTemplateService;

	@Autowired
	private SpringFXMLLoader loader;

	@FXML
	private TextArea leftQuery;

	@FXML
	private TextArea rightQuery;

	@FXML
	private Button executeButton;

	public void handleExecuteClick() {
		String left = this.leftQuery.getText();
		String right = this.rightQuery.getText();

		if ((left == null && right == null)
				|| (left.isEmpty() && right.isEmpty())) {
			throw new IllegalStateException(
					"You must provide at least one query!");
		}

		if (left == null || left.isEmpty()) {
			left = right;
		} else if (right == null || right.isEmpty()) {
			right = left;
		}

		try {
			showResults(this.jdbcTemplateService.doQueryOnLeft(left),
					this.jdbcTemplateService.doQueryOnRight(right));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void showResults(List<Map<String, Object>> left,
			List<Map<String, Object>> right) {
		try {
			BorderPane resultsView = this.loader
					.load("/com/querier/view/ResultsView.fxml");

			Scene scene = new Scene(resultsView);
			Stage stage = new Stage();

			stage.setScene(scene);
			stage.show();

			ResultsController controller = loader.getController();

			controller.setResults(left, right);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Stage getPrimaryStage() {
		return this.primaryStage;
	}

	@Override
	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}
}
