package com.querier.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.beans.value.ObservableValueBase;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import org.springframework.stereotype.Controller;

import com.querier.util.ObjectWrapperWithDiffFlag;

@Controller
public class ResultsController implements BaseSceneController {

	protected Stage primaryStage;

	@FXML
	private TableView<Map<String, ObjectWrapperWithDiffFlag>> leftResults;

	@FXML
	private TableView<Map<String, ObjectWrapperWithDiffFlag>> rightResults;

	@FXML
	private Button executeButton;

	public void setResults(List<Map<String, Object>> left,
			List<Map<String, Object>> right) {

		List<Map<String, ObjectWrapperWithDiffFlag>> leftDiff = new ArrayList<Map<String, ObjectWrapperWithDiffFlag>>();
		List<Map<String, ObjectWrapperWithDiffFlag>> rightDiff = new ArrayList<Map<String, ObjectWrapperWithDiffFlag>>();

		Set<String> keys = new HashSet<String>();
		keys.addAll(left.get(0).keySet());
		keys.addAll(right.get(0).keySet());
		
		int min = left.size() > right.size() ? right.size() : left.size();
		
		for (int i = 0; i < min; i++) {
			Map<String, Object> lm = left.get(i);
			Map<String, Object> rm = right.get(i);

			Map<String, ObjectWrapperWithDiffFlag> lRow = new HashMap<String, ObjectWrapperWithDiffFlag>(
					keys.size());
			Map<String, ObjectWrapperWithDiffFlag> rRow = new HashMap<String, ObjectWrapperWithDiffFlag>(
					keys.size());

			for (String key : keys) {
				Object l = lm.get(key);
				Object r = rm.get(key);

				if (l != null) {
					if (l.equals(r)) {
						lRow.put(key, new ObjectWrapperWithDiffFlag(l, false));
						rRow.put(key, new ObjectWrapperWithDiffFlag(r, false));
					} else {
						lRow.put(key, new ObjectWrapperWithDiffFlag(l, true));
						rRow.put(key, new ObjectWrapperWithDiffFlag(r, true));
					}
				} else if (r == null) {
					lRow.put(key, new ObjectWrapperWithDiffFlag(l, false));
					rRow.put(key, new ObjectWrapperWithDiffFlag(r, false));
				} else {
					lRow.put(key, new ObjectWrapperWithDiffFlag(l, true));
					rRow.put(key, new ObjectWrapperWithDiffFlag(r, true));
				}
			}

			leftDiff.add(lRow);
			rightDiff.add(rRow);
		}
		
		List<Map<String, Object>> add;
		List<Map<String, ObjectWrapperWithDiffFlag>> ins;
		if (left.size() > min) {
			add = left;
			ins = leftDiff;
		} else {
			add = right;
			ins = rightDiff;
		}
		
		for (int i = min; i < add.size(); i++) {
			Map<String, Object> m = right.get(i);

			Map<String, ObjectWrapperWithDiffFlag> row = new HashMap<String, ObjectWrapperWithDiffFlag>(
					keys.size());
			
			for (String key : keys) {
				row.put(key, new ObjectWrapperWithDiffFlag(m.get(key), true));
			}
			
			ins.add(row);
		}
		
		leftResults.getColumns().clear();
		
		for (String key : left.get(0).keySet()) {
			leftResults.getColumns().add(getTableColumn(key));
		}
		
		leftResults.setItems(FXCollections.observableArrayList(leftDiff));
		
		rightResults.getColumns().clear();
		
		for (String key : right.get(0).keySet()) {
			rightResults.getColumns().add(getTableColumn(key));
		}
		
		rightResults.setItems(FXCollections.observableArrayList(rightDiff));
		
	}

	@Override
	public Stage getPrimaryStage() {
		return this.primaryStage;
	}

	@Override
	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}
	
	private TableColumn<Map<String, ObjectWrapperWithDiffFlag>, ObjectWrapperWithDiffFlag> getTableColumn(String key) {
		TableColumn<Map<String, ObjectWrapperWithDiffFlag>, ObjectWrapperWithDiffFlag> column = new TableColumn<Map<String, ObjectWrapperWithDiffFlag>, ObjectWrapperWithDiffFlag>(
				key);
		column.setText(key);
		
		column.setCellFactory(param -> { 
			return new TableCell<Map<String, ObjectWrapperWithDiffFlag>, ObjectWrapperWithDiffFlag>() {

				@Override
				public void updateItem(ObjectWrapperWithDiffFlag item,
						boolean empty) {
					super.updateItem(item, empty);
					
					if (item != null) {
						setText(item.toString());
						if (item.isDifferent) {
							this.setTextFill(Color.RED);
						} else {
							this.setTextFill(Color.BLACK);
						}
					}
				}
			};
		});
		
		column.setCellValueFactory(param -> {
			ObjectWrapperWithDiffFlag obj;

			if (param.getValue().size() > 0) {
				obj = param.getValue().get(param.getTableColumn().getText());
			} else {
				obj = null;
			}

			return new ObservableValueBase<ObjectWrapperWithDiffFlag>() {
				@Override
				public ObjectWrapperWithDiffFlag getValue() {
					return obj;
				}
			};
		});
		
		return column;
	}
}
