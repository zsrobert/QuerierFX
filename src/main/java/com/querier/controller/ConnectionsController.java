package com.querier.controller;

import java.io.IOException;
import java.sql.SQLException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.querier.model.Connection;
import com.querier.service.def.ConnectionService;
import com.querier.service.def.JdbcTemplateService;
import com.querier.util.SpringFXMLLoader;

@Controller
public class ConnectionsController implements BaseSceneController {

	protected Stage primaryStage;

	@Autowired
	private ConnectionService connectionService;

	@Autowired
	private JdbcTemplateService jdbcTemplateService;

	@Autowired
	private SpringFXMLLoader loader;

	@FXML
	private ListView<Connection> leftConnections;

	@FXML
	private ListView<Connection> rightConnections;

	@FXML
	private Button connectButton;

	@FXML
	private Button addButton;

	@FXML
	private Button editButton;

	@FXML
	private Button deleteButton;

	private ListView<Connection> selectedConnections;

	public void handleAddConnectionClick() {
		showConnectionAddEditDialog(new Connection(), false);
	}

	public void handleEditConnectionClick() {
		showConnectionAddEditDialog(this.selectedConnections
				.getSelectionModel().getSelectedItem(), true);
	}

	public void handleDeleteConnectionClick() {
		Connection connection = this.selectedConnections.getSelectionModel()
				.getSelectedItem();
		this.connectionService.delete(connection.getId());
	}

	public void handleConnectClick() {
		Connection left = this.leftConnections.getSelectionModel()
				.getSelectedItem();
		Connection right = this.rightConnections.getSelectionModel()
				.getSelectedItem();

		if (left == null || right == null) {
			throw new IllegalStateException("You must select both connections!");
		}

		try {
			this.jdbcTemplateService.connectLeft(left);
			this.jdbcTemplateService.connectRight(right);

			BorderPane queriesView = this.loader.load("/com/querier/view/QueriesView.fxml");

			Scene scene = new Scene(queriesView);

			this.primaryStage.setScene(scene);
			this.primaryStage.show();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}

	}

	private void updateConnectionsListViews() {
		this.leftConnections.setItems(this.connectionService.findAll());
		this.rightConnections.setItems(this.connectionService.findAll());
	}

	@FXML
	private void initialize() {
		updateConnectionsListViews();

		this.leftConnections.focusedProperty().addListener(
				new ChangeListener<Boolean>() {

					@Override
					public void changed(
							ObservableValue<? extends Boolean> observable,
							Boolean oldValue, Boolean newValue) {
						ConnectionsController.this.selectedConnections = ConnectionsController.this.leftConnections;
					}
				});

		this.rightConnections.focusedProperty().addListener(
				new ChangeListener<Boolean>() {

					@Override
					public void changed(
							ObservableValue<? extends Boolean> observable,
							Boolean oldValue, Boolean newValue) {
						ConnectionsController.this.selectedConnections = ConnectionsController.this.rightConnections;
					}
				});
	}

	private boolean showConnectionAddEditDialog(Connection connection,
			boolean isEdit) {
		try {
			BorderPane page = (BorderPane) this.loader
					.load("view/AddEditConnectionPopupView.fxml");

			Stage dialogStage = new Stage();
			dialogStage.setTitle(isEdit ? "Edit connection" : "Add connection");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(this.primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			AddEditConnectionPopupController controller = loader
					.getController();
			controller.setDialog(dialogStage);
			controller.setConnection(connection);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Stage getPrimaryStage() {
		return this.primaryStage;
	}

	@Override
	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}
}
