package com.querier.controller;

import javafx.stage.Stage;

public interface BaseSceneController {
	

	public Stage getPrimaryStage();

	public void setPrimaryStage(Stage primaryStage);
}
