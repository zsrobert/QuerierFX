package com.querier.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.querier.model.Connection;
import com.querier.service.def.ConnectionService;

@Controller
public class AddEditConnectionPopupController extends BaseDialogController {

	@Autowired
	private ConnectionService connectionService;

	@FXML
	private TextField nameTextField;

	@FXML
	private TextField hostnameTextField;

	@FXML
	private TextField portTextField;

	@FXML
	private TextField schemaTextField;

	@FXML
	private TextField usernameTextField;

	@FXML
	private TextField passwordTextField;

	@FXML
	private Button saveButton;

	@FXML
	private Button cancelButton;

	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
		this.nameTextField.setText(this.connection.getName());
		this.hostnameTextField.setText(this.connection.getHostname());
		this.portTextField.setText(Long.toString(this.connection.getPort()));
		this.schemaTextField.setText(this.connection.getSchema());
		this.usernameTextField.setText(this.connection.getUsername());
		this.passwordTextField.setText(this.connection.getPassword());
	}

	@FXML
	private void handleSaveClicked() {
		this.connection.setName(this.nameTextField.getText());
		this.connection.setHostname(this.hostnameTextField.getText());
		this.connection.setPort(Long.parseLong(this.portTextField.getText()));
		this.connection.setSchema(this.schemaTextField.getText());
		this.connection.setUsername(this.usernameTextField.getText());
		this.connection.setPassword(this.passwordTextField.getText());

		this.connectionService.save(this.connection);
		this.dialog.close();
	}

	@FXML
	private void handleCancelClicked() {
		this.dialog.close();
	}

}
