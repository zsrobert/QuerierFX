package com.querier.controller;

import javafx.stage.Stage;

public class BaseDialogController {
	protected Stage dialog;

	public Stage getDialog() {
		return dialog;
	}

	public void setDialog(Stage dialog) {
		this.dialog = dialog;
	}

}
